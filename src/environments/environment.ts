// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase:{
    apiKey: "AIzaSyCh_IQIOoyW05DbAj-SlUHrdGXBRN4l3eg",
    authDomain: "homework4-4fa3b.firebaseapp.com",
    databaseURL: "https://homework4-4fa3b.firebaseio.com",
    projectId: "homework4-4fa3b",
    storageBucket: "homework4-4fa3b.appspot.com",
    messagingSenderId: "358843698228"
  }
  
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
